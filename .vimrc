" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'elixir-editors/vim-elixir'
Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdcommenter'
Plug 'mileszs/ack.vim'
Plug 'dense-analysis/ale'
Plug 'Raimondi/delimitMate'
Plug 'honza/vim-snippets'
Plug 'SirVer/ultisnips'
Plug 'leafgarland/typescript-vim' " TypeScript syntax
Plug 'maxmellon/vim-jsx-pretty'   " JS and JSX syntax

" Initialize plugin system
call plug#end()

" Because of the fucking hack in the NodeJs
set noswapfile

" Activamos el resaltado de sintaxis
syntax on

"" Activamos la barra de informacion
set ruler

"" Desactivamos la compatibilidad con VI
set nocompatible

"" El identado lo hace VIM
set autoindent

" opciones de tabulacion. 4 espacios
set incsearch
set expandtab

" Typescript files
" Recognize .tsx files as TypeScript files
autocmd BufRead,BufNewFile *.tsx set filetype=typescript

autocmd FileType html setlocal shiftwidth=2 tabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2
autocmd FileType sql setlocal shiftwidth=2 tabstop=2
autocmd FileType tf setlocal shiftwidth=2 tabstop=2
autocmd FileType json setlocal shiftwidth=2 tabstop=2
autocmd FileType c setlocal shiftwidth=4 tabstop=4
autocmd FileType cpp setlocal shiftwidth=4 tabstop=4

"comportamiento de backspace
set backspace=indent,eol,start

" To solve some issues with rxvt-unicode
set t_Co=256

"tema de colores
colorscheme shine

" Intentation
filetype indent on

"tipo de fichero
filetype plugin on

" Command history
set history=50

" ctrlp starting directory
" let g:ctrlp_working_path_mode = 'c'

" Highlight trailing whitespaces
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

"" Elixir
"" Syntax highlight
au BufRead,BufNewFile *.ex,*.exs set filetype=elixir
au BufRead,BufNewFile *.eex,*.heex,*.leex,*.sface,*.lexs set filetype=eelixir
au BufRead,BufNewFile mix.lock set filetype=elixir

" Required, explicitly enable Elixir LS
let g:ale_linters={'elixir': ['elixir-ls']}

" Required, tell ALE where to find Elixir LS
let g:ale_elixir_elixir_ls_release="$HOME/.local/share/elixir-ls/rel"

" Optional, you can disable Dialyzer with this setting
let g:ale_elixir_elixir_ls_config={'elixirLS': {'dialyzerEnabled': v:false}}

" Automatically format on saving for vim-mix-format
let g:ale_fixers ={'elixir': ['mix_format']}

" Optional, configure as-you-type completions
set completeopt=menu,menuone,preview,noselect,noinsert
let g:ale_completion_enabled=1
let g:ale_lint_on_save=1
let g:ale_fix_on_save=1

"" Use the silver searcher
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" Map Ack to Ag I'm more used to the latter
cnoreabbrev Ag Ack

" Go to definition
map <leader>g :ALEGoToDefinition<CR>

" Find references
map <leader>r :ALEFindReferences<CR>

" Shortcut to close all buffers but the current one
command!  BufOnly execute '%bdelete|edit #|normal `"'
