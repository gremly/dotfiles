# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Aliases
alias ls='ls --color=auto'
alias more='less'
alias grep='grep --color'

# Git aliases
alias gst='git status'
alias gco='git checkout'
alias gci='git commit -m'
alias ga='git add'
alias gap='git add -p'
alias gdf='git diff'
alias gl='git log'

# Radio stations
alias hhrock='mplayer https://s5-webradio.webradio.de/rockantenne-hamburg'
alias ukradio='mplayer https://edge-bauerall-01-gos2.sharp-stream.com/absoluteradio.mp3'
alias jzradio='mplayer http://us2.internet-radio.com:8443/'
alias crradio='mplayer http://217.114.200.100:80/'
alias pradio='mplayer "http://144.76.106.52:7000/progressive.mp3?icy=http"'
alias tradio='mplayer "http://213.251.190.165/pulstranceHD.mp3?icy=http"'

# Env vars
# JAVA_HOME
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk

# Prompt theme enabled
autoload -U promptinit
autoload -U colors && colors
promptinit
prompt bart cyan green yellow

# SSH agent auto addition
if ! pgrep -u $USER ssh-agent > /dev/null; then
#if ! ps ux | grep $USER | grep ssh-agent | grep -v grep | head -n 1 | awk '{ print $2 }' > /dev/null; then
    ssh-agent > ~/.ssh-agent-thing
fi
if [[ "$SSH_AGENT_PID" == "" ]]; then
    eval $(<~/.ssh-agent-thing) > /dev/null
fi
ssh-add -l > /dev/null || alias ssh='ssh-add -l >/dev/null || ssh-add && unalias ssh; ssh'

# Local bin added to path
export PATH="$HOME/.local/bin:$PATH"

# Kerl options
export KERL_CONFIGURE_OPTIONS="--enable-hipe --enable-kernel-poll --with-wx --enable-threads --enable-sctp --enable-smp-support --enable-dirty-schedulers"

# Erl flags
export ERL_AFLAGS="-kernel shell_history enabled"

# Kiex source
test -s "$HOME/.kiex/scripts/kiex" && source "$HOME/.kiex/scripts/kiex"

# Asdf with pacman
. $HOME/.asdf/asdf.sh

# Flutter
export CHROME_EXECUTABLE=/usr/bin/chromium
